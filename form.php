<?php

	/**
		* Description: This code creates an advanced search for Wordpress Woocommerce
		* Version: 1.0.0
		* Author: SG Media
		* Author URI: http://fb.com/sgmediaiasi
	**/

?>

<form role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<input type="hidden" name="post_type" value="product" />
	<select name="category">
		<option value="">Select a category</option>
		<?php
			//Display ascendant all categories even those which don't have products
            $terms = get_terms( 'product_cat', 'order=ASC&hide_empty=0' );
            foreach($terms as $term){
                echo '<option value="' . $term->slug . '">' . $term->name . '</option>';
            }
        ?>
	</select>
	<input type="search" name="s" />
	<button type="submit">Submit</button>
</form>

<?php

	//Copy and paste this code anywhere in your Wordpress Site

?>