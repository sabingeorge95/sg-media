<?php

/**
	* Description: This code creates an advanced search for Wordpress Woocommerce
	* Version: 1.0.0
	* Author: SG Media
	* Author URI: http://fb.com/sgmediaiasi
**/

/**
	* add new query vars into global array $qvars
	* @param $qvars
	* @return array
*/

	function parametersQueryVars($qvars) {
		$qvars[] = 'category';
		return $qvars;
	}
	add_filter('query_vars', 'parametersQueryVars');

/**
	* after search in WP_Query will be all products with that category
	* @param $query
	* @return object
*/

	function advancedSearchQuery($query) {
		if (!is_search() && !is_archive())
			return;
		$category = urldecode(get_query_var('category')) ? urldecode(get_query_var('category')) : '';
		if ($category) {
			$myQuery[] = array(
				'taxonomy' => 'product_cat',
				'field' => 'slug',
				'terms' => $category //if would have been more -> array('term1', 'term2' etc)
			);
		}
		$query->set('tax_query', $myQuery);
		return $query;
	}
	add_filter('pre_get_posts', 'advanced_search_query')

/**
	* Rewrite rule, that after search WP redirects at a URI of that form: domain/category/s (e.g: domain/sport/shirt)
*/

	function rewriteRule() {
		$regex = '([^/]+)/([^/]+)/?$';
		add_rewrite_rule($regex,'index.php?post_type=product&category=$matches[1]&s=$matches[2]','top');
	}
	add_action('init', 'rewriteRule', 10, 0);

/**
	* I created a redirect
*/

	function redirectAfterSearch() {
		if (is_search() || is_archive()) {
			$s = $_GET['s'] ? $_GET['s'] : false;
			$category = $_GET['category'] ? $_GET['category'] : false;
			$url = '/';
			if ($s && $category) {
				$url .= $category . '/' . $s;
			}
			if ($url != '/') {
				wp_redirect(get_bloginfo('url') . $url);
		    	exit();
			}
		}
	}
	add_action('template_redirect', 'redirectAfterSearch');

//ATTENTION: Copy and paste this code into your functions.php file. Now go to Dashboard -> Settings -> Permalink -> Check Post Name -> Save.